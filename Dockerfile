FROM rails
MAINTAINER Christophe Robillard <geek@robiweb.net>
RUN apt-get -qq update
RUN apt-get -qqy install imagemagick
RUN apt-get -qqy install vim
